@if not exist %1 goto MKDIR
@for /F %%i in ('dir /b %1') do {
	@goto NOTEMPTY
}
goto COPY

:MKDIR
@mkdir %1

:COPY
xcopy /s /e /v /exclude:excludedfileslist.txt %~d0 %1
@goto DONE

:NOTEMPTY
@echo %1 existe deja et n'est pas vide

:DONE